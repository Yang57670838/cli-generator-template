const {
    Command
} = require('commander');
const program = new Command();
var prompt = require('prompt');
var colors = require('colors');
const fs = require('fs');
const path = require('path');
const fse = require('fs-extra')

let fn;
let isDep
let isIma
let isNg;

program
    .command('start [suffix]')
    .action(function (suffix) {
        prompt.start();
        console.log('Lets provide some informations'.green);
        prompt.get(['filename'], function (err, r) {
            if (r.filename) {
                fn = r.filename;
            } else {
                fn = 'project-template';
            }
            fs.mkdir(path.join(__dirname, `/../${fn}`), (err) => {
                if (err) {
                    throw err;
                }
                console.log("directory is created.".green);
                if (suffix === 'all') {
                    console.log('you will setup with all default features'.underline.red);
                    copyFiles(true, true, true);
                } else {
                    console.log('please tell which contents do you want'.green);
                    console.log('you can choose yes or no, or just hit enter for default yes\n'.blue);
                    prompt.get(['deployment', 'nginx', 'image'], function (err, result) {
                        if (result.deployment === '' || result.deployment.toLowerCase() === 'yes' || result.deployment.toLowerCase() === 'y') {
                            isDep = true;
                        } else {
                            isDep = false;
                        }
                        if (result.nginx === '' || result.nginx.toLowerCase() === 'yes' || result.nginx.toLowerCase() === 'y') {
                            isNg = true;
                        } else {
                            isNg = false;
                        }
                        if (result.image === '' || result.image.toLowerCase() === 'yes' || result.image.toLowerCase() === 'y') {
                            isIma = true;
                        } else {
                            isIma = false;
                        }
                        copyFiles(isDep, isNg, isIma);
                    })
                }
            });
        })
    });

program.parse(process.argv)

async function copyFiles(isDeployment, isNginx, isImage) {
    try {
        await fse.copy('./template/client', path.join(__dirname, `/../${fn}`))
        console.log('base template is copied.'.green);
        if (isDeployment) {
            await fse.copy('./template/openshift', path.join(__dirname, `/../${fn}/openshift`))
            console.log('deployment template is copied.'.green);
        }
        if (isNginx) {
            await fse.copy('./template/nginx', path.join(__dirname, `/../${fn}`))
            console.log('nginx configure template is copied.'.green);
        }
        if (isImage) {
            await fse.copy('./template/image', path.join(__dirname, `/../${fn}`))
            console.log('docker image template is copied.'.green);
        }
        console.log('Template is created.'.green);
    } catch (err) {
        console.error('copy files failed..')
    }
}